import os
import sys
import glob
import json
import xmltodict

from collections import OrderedDict
from shutil import copyfile

from lxml import etree
from rdflib import Graph

from dagster import (
    Any,
    Bool,
    Field,
    List,
    Output,
    Nothing,
    LocalFileHandle,
    PipelineDefinition,
    PipelineExecutionResult,
    PresetDefinition,
    String,
    execute_pipeline,
    pipeline,
    solid,
    InputDefinition,
)
from dagster.core.execution.context.compute import SolidExecutionContext
from dagster.core.definitions.output import OutputDefinition

from common.dagster_utils import preset_defs


rdf_enrichment_config = {
    "edm_dir": Field(String),
    "rdf_dir": Field(String),
    "out_dir": Field(String),
}

# for parsing rdf ttl
IDENTIFIER_KEY = "http://www.europeana.eu/schemas/edm/identifier"
RDF_ABOUT = "{http://www.w3.org/1999/02/22-rdf-syntax-ns#}about"
PROVIDED_CHO = "{http://www.europeana.eu/schemas/edm/}ProvidedCHO"

service_keys = ["ns1:id", "ns1:type", "edm:identifier"]
attr_map = {"owl:sameAs": "@rdf:resource"}
node_transform_map = {
    "gndo:gndIdentifier": "dc:identifier",
    "jl:birthLocation": "rdaGr2:placeOfBirth",
    "jl:deathLocation": "rdaGr2:placeOfDeath",
    "jl:hasAbstract": "rdaGr2:biographicalInformation",
    "jl:hasPublication": "skos:note",
}


def collect_ttl_ids(rdf_dir, logger):
    items_map = {}
    items_map_to_return = {}
    for rdf_file in sorted(glob.glob(os.path.join(rdf_dir, "*ttl"))):
        graph = Graph()
        graph.load(rdf_file, format="turtle")
        ser = graph.serialize(format="json-ld")

        for rdf_data in json.loads(ser):
            if IDENTIFIER_KEY not in rdf_data.keys():
                continue
            for idn in rdf_data[IDENTIFIER_KEY]:
                rec_id = idn["@value"]

                if rec_id not in items_map:
                    items_map[rec_id] = {}
                    items_map_to_return[rec_id] = {}

                rdf_root_node = graph.namespace_manager.qname(rdf_data["@type"][0])

                if rdf_root_node not in items_map[rec_id]:
                    items_map[rec_id][rdf_root_node] = {}
                    items_map_to_return[rec_id][rdf_root_node] = {}

                try:
                    new_node = next(
                        filter(
                            lambda node: node["@rdf:about"] == rdf_data["@id"],
                            items_map[rec_id][rdf_root_node].values(),
                        )
                    )
                except StopIteration:
                    new_node = {"@rdf:about": rdf_data["@id"], "weight": 0}

                for key, values in rdf_data.items():
                    qname = check_transform_node(graph.namespace_manager.qname(key))
                    if qname in service_keys:
                        continue

                    for v_dict in values:
                        for k, v in v_dict.items():
                            if not v:
                                continue

                            if qname not in new_node:
                                new_node[qname] = []

                            if type(v) is str:
                                if qname in attr_map.keys():
                                    new_node[qname].append({attr_map[qname]: v})
                                else:
                                    new_node[qname].append(v)
                            else:
                                # it's a dict
                                new_node[qname].append(v["@value"])

                            new_node["weight"] += 1

                # Cleaning up duplicates via skos:prefLabel
                try:
                    pref_label = new_node["skos:prefLabel"][0]
                except Exception as e:
                    logger.warning(
                        f"Error occured in 'collect_ttl_ids' {e} /n new node: {new_node}"
                    )
                if pref_label not in items_map[rec_id][rdf_root_node]:
                    items_map[rec_id][rdf_root_node][pref_label] = new_node
                    items_map_to_return[rec_id][rdf_root_node] = new_node
                else:
                    existing_node = items_map[rec_id][rdf_root_node][pref_label]
                    if new_node["weight"] > existing_node["weight"] or (
                        new_node["weight"] == existing_node["weight"]
                        and new_node["@rdf:about"] < existing_node["@rdf:about"]
                    ):
                        items_map[rec_id][rdf_root_node][pref_label] = new_node
                        items_map_to_return[rec_id][rdf_root_node] = new_node

    logger.info(f"Number of records to enrich: {len(items_map)}")
    return items_map, items_map_to_return


def convert_etree_to_dict(record):
    record_string = etree.tostring(record, xml_declaration=False).decode("utf-8")
    record_ordered_dict = xmltodict.parse(record_string)
    record_json = json.loads(json.dumps(record_ordered_dict))
    return record_json


def check_transform_node(qname):
    return node_transform_map[qname] if qname in node_transform_map else qname


def make_rdf_enrichment(edm_dir, rdf_dir, out_dir, logger):
    set_name = rdf_dir.split("/")[1]

    # store number of enriched entities
    stats = {"edm:Agent": 0, "edm:Place": 0}
    total_files_updated = 0
    total_rec_num = 0
    total_rec_num_updated = 0

    # step 1: collecting all ids from TTL files
    ttl_ids, ttl_ids_proper = collect_ttl_ids(rdf_dir, logger)

    # iterating over edm dump
    for file_num, xml_fn in enumerate(sorted(glob.glob(f"{edm_dir}/*xml"))):
        # if not enriched -> just copy
        # if enriched -> change and save to new dir

        xml_file = open(xml_fn, "rb")
        doc = xmltodict.parse(xml_file)
        was_enriched = False
        new_fn = os.path.join(out_dir, xml_fn.split("/")[-1])

        if os.path.exists(new_fn):
            logger.info(f"File {new_fn} exists - skip enrichment!")
            continue

        # iteration over records inside one xml file
        if not doc["OAI-PMH"]["ListRecords"]:
            continue
        for rec_num, record in enumerate(doc["OAI-PMH"]["ListRecords"]["record"]):
            if "metadata" not in record.keys():  # could be deleted
                continue
            provided_cho = record["metadata"]["rdf:RDF"]["edm:ProvidedCHO"]
            provided_cho_rdf_about = provided_cho["@rdf:about"]

            if "guf" in set_name.lower():  # one of the GUF sets
                provided_cho_rdf_about = (
                    "http://nbn-resolving.de/" + record["header"]["identifier"]
                )

            if provided_cho_rdf_about in ttl_ids.keys():  # means it have enrichment
                was_enriched = True
                from_ttl_ids_proper = ttl_ids_proper[provided_cho_rdf_about]
                # adding new elems as ordered dicts
                for (
                    key
                ) in (
                    from_ttl_ids_proper.keys()
                ):  # loop because ordereddict could have two roots
                    from_ttl_ids_proper[key].pop(
                        "weight"
                    )  # remove service weight field
                    unparsed_from_ttl_ids = xmltodict.unparse(
                        {key: from_ttl_ids_proper[key]}
                    )

                    parsed_from_ttl_ids = xmltodict.parse(unparsed_from_ttl_ids)
                    doc["OAI-PMH"]["ListRecords"]["record"][rec_num]["metadata"][
                        "rdf:RDF"
                    ].update(parsed_from_ttl_ids)
                    stats[key] += 1  # update stats

                    # adding needed namespace to rdf:
                    record_meta_unparsed = xmltodict.unparse(record["metadata"])
                    record_meta_unparsed = record_meta_unparsed.replace(
                        "<rdf:RDF>",
                        '<rdf:RDF xmlns:rdaGr2="http://rdvocab.info/ElementsGr2/" xmlns:wgs84_pos="http://www.w3.org/2003/01/geo/wgs84_pos#">',
                    )
                    record_meta_parsed = xmltodict.parse(record_meta_unparsed)
                    doc["OAI-PMH"]["ListRecords"]["record"][rec_num][
                        "metadata"
                    ] = record_meta_parsed

                total_rec_num_updated += 1

        total_rec_num += rec_num

        if was_enriched:
            doc_to_write = xmltodict.unparse(doc, pretty=True)
            with open(new_fn, "w+") as new_xml:
                new_xml.write(doc_to_write)
            total_files_updated += 1
        else:  # just copy
            copyfile(xml_fn, new_fn)

    logger.info(
        f"Totally enriched: {total_rec_num_updated} out of {total_rec_num+1} records in {set_name} collection. \n\n While enrichment were added: \n - {stats['edm:Agent']} edm:Agent entities \n - {stats['edm:Place']} edm:Place entities"
    )


@solid(
    config_schema=rdf_enrichment_config,
    output_defs=[OutputDefinition(name="result", dagster_type=List[LocalFileHandle])],
)
def rdf_enrichment(context: SolidExecutionContext):
    conf = context.solid_config
    logger = context.log
    out_files = []

    edm_dir = os.path.join(conf["edm_dir"])  # folder with edm dump
    rdf_dir = os.path.join(conf["rdf_dir"])  # folder with ttl
    out_dir = os.path.join(conf["out_dir"])  # folder to write enriched files
    if not os.path.exists(rdf_dir):
        context.log.info("RDF files path doesn't exist, enrichment aborted.")
        yield Output(False, "result")
        return

    if not os.path.exists(out_dir):
        os.makedirs(out_dir, exist_ok=True)

    make_rdf_enrichment(edm_dir, rdf_dir, out_dir, logger)

    for enriched_fn in sorted(glob.glob(f"{out_dir}/*xml")):
        out_files.append(LocalFileHandle(enriched_fn))

    yield Output(out_files, "result")


@pipeline(preset_defs=preset_defs(["conf/pipelines/rdf"]))
def rdf_enrichment_pipeline():
    return rdf_enrichment()


### YIVO ENRICHMENT PART
# for parsing yivo ttl
SKOS_PREF_LABEL = "http://www.w3.org/2004/02/skos/core#prefLabel"
SKOS_RELATED = "http://www.w3.org/2004/02/skos/core#related"
SKOS_ALT_LABEL = "http://www.w3.org/2004/02/skos/core#altLabel"
DESCRIBED_AT = "http://data.judaicalink.org/ontology/describedAt"
HAS_ABSTRACT = "http://data.judaicalink.org/ontology/hasAbstract"

ttl2edm_mapping = {
    SKOS_ALT_LABEL: "skos:altLabel",
    DESCRIBED_AT: "owl:sameAs",
    HAS_ABSTRACT: "skos:note",
}

yivo_enrichment_config = {
    "path_to_yivo_ttl": Field(String),
    "in_dir": Field(String),
    "out_yivo_dir": Field(String),
}


def collect_yivo_ttl_map(path_to_yivo_ttl, logger):
    # read ttl skos labels
    logger.info(f"Collecting labels from yivo ttl: {path_to_yivo_ttl}")
    ttl_labels = {}
    graph = Graph()
    graph.load(path_to_yivo_ttl, format="turtle")
    ser = graph.serialize(format="json-ld")
    for n, rdf_data in enumerate(json.loads(ser)):
        # print(rdf_data)
        prefLabel = rdf_data[SKOS_PREF_LABEL][0]["@value"]
        id_link = rdf_data["@id"]
        dict_for_edm = {}

        for ttl_key, edm_key in ttl2edm_mapping.items():
            if ttl_key in rdf_data.keys():
                if ttl_key == DESCRIBED_AT:
                    dict_for_edm.update(
                        {edm_key: [i["@id"] for i in rdf_data[ttl_key]]}
                    )
                else:
                    dict_for_edm.update(
                        {
                            edm_key: [
                                i["@value"]
                                for i in rdf_data[ttl_key]
                                if i["@value"] != prefLabel
                            ]
                        }
                    )

        if "owl:sameAs" in dict_for_edm.keys():
            dict_for_edm["owl:sameAs"] += [id_link]
        else:
            dict_for_edm["owl:sameAs"] = [id_link]

        ttl_labels.update({prefLabel: dict_for_edm})

    logger.info(f"total number of entities in ttl: {n+1}")

    return ttl_labels


def make_yivo_enrichment(edm_enriched_dir, out_dir, path_to_yivo_ttl, logger):
    ttl_labels = collect_yivo_ttl_map(path_to_yivo_ttl, logger)
    # store number of enriched entities
    logger.info("Finding entities from record in ttl labels ... ")

    stats = {"edm:Agent": 0, "edm:Place": 0}

    tot_found = 0
    tot_found_list = []
    updated_ids = []

    for file_num, xml_fn in enumerate(sorted(glob.glob(f"{edm_enriched_dir}/*xml"))):
        # if not enriched -> just copy
        # if enriched -> change and save to new dir

        xml_file = open(xml_fn, "rb")
        doc = xmltodict.parse(xml_file)
        is_enriched = False

        # iteration over records inside one xml file
        for rec_num, record in enumerate(doc["OAI-PMH"]["ListRecords"]["record"]):
            enriched_objects = []

            if "metadata" not in record.keys():
                continue

            rec_id = record["header"]["identifier"]

            rdf_of_record = record["metadata"]["rdf:RDF"]
            rdf_of_record_keys = rdf_of_record.keys()

            for key in stats.keys():
                if key in rdf_of_record_keys:
                    enriched_objects.append(key)

            if not enriched_objects:
                continue

            for enriched_obj in enriched_objects:
                # get name which have to be found
                if isinstance(rdf_of_record[enriched_obj], list):
                    continue

                if "skos:prefLabel" not in rdf_of_record[enriched_obj].keys():
                    continue

                pref_Label_from_record = str(
                    rdf_of_record[enriched_obj]["skos:prefLabel"]
                )
                if pref_Label_from_record in ttl_labels.keys():
                    tot_found += 1
                    tot_found_list.append(pref_Label_from_record)

                    record_enriched_part = doc["OAI-PMH"]["ListRecords"]["record"][
                        rec_num
                    ]["metadata"]["rdf:RDF"][enriched_obj]

                    updated_dict = ttl_labels[pref_Label_from_record]

                    for edm_key in updated_dict.keys():
                        print(f"Updated record: {rec_id}")
                        updated_ids.append(rec_id)
                        if not updated_dict[edm_key]:
                            continue
                        if edm_key in list(record_enriched_part.keys()):
                            if not isinstance(record_enriched_part[edm_key], list):
                                doc["OAI-PMH"]["ListRecords"]["record"][rec_num][
                                    "metadata"
                                ]["rdf:RDF"][enriched_obj][edm_key] = [
                                    doc["OAI-PMH"]["ListRecords"]["record"][rec_num][
                                        "metadata"
                                    ]["rdf:RDF"][enriched_obj][edm_key]
                                ]
                            if edm_key == "owl:sameAs":
                                for i in updated_dict[edm_key]:
                                    doc["OAI-PMH"]["ListRecords"]["record"][rec_num][
                                        "metadata"
                                    ]["rdf:RDF"][enriched_obj][edm_key].append(
                                        OrderedDict({"@rdf:resource": i})
                                    )
                            else:
                                doc["OAI-PMH"]["ListRecords"]["record"][rec_num][
                                    "metadata"
                                ]["rdf:RDF"][enriched_obj][edm_key].append(
                                    updated_dict[edm_key]
                                )
                        else:
                            if edm_key == "owl:sameAs":
                                to_upd = [
                                    OrderedDict({"@rdf:resource": i})
                                    for i in updated_dict[edm_key]
                                ]
                                doc["OAI-PMH"]["ListRecords"]["record"][rec_num][
                                    "metadata"
                                ]["rdf:RDF"][enriched_obj][edm_key] = to_upd
                            else:
                                doc["OAI-PMH"]["ListRecords"]["record"][rec_num][
                                    "metadata"
                                ]["rdf:RDF"][enriched_obj][edm_key] = updated_dict[
                                    edm_key
                                ]

                    stats[enriched_obj] += 1
                    is_enriched = True
                else:
                    continue

        new_fn = os.path.join(out_dir, xml_fn.split("/")[-1])
        if is_enriched:
            doc_to_write = xmltodict.unparse(doc, pretty=True)
            with open(new_fn, "w+") as new_xml:
                new_xml.write(doc_to_write)
        else:
            copyfile(xml_fn, new_fn)

    logger.info(f"List of updated ids: {updated_ids}")
    logger.info(f"YIVO enrichment applied for {len(tot_found_list)} records")


@solid(
    config_schema=yivo_enrichment_config,
    input_defs=[InputDefinition("nothing", dagster_type=Nothing)],
    output_defs=[OutputDefinition(name="result", dagster_type=List[LocalFileHandle])],
)
def yivo_enrichment(context: SolidExecutionContext):
    conf = context.solid_config
    logger = context.log
    out_files = []

    path_to_yivo_ttl = os.path.join(conf["path_to_yivo_ttl"])  # full path to uvio ttl
    in_dir = os.path.join(conf["in_dir"])  # folder with enriched data
    out_yivo_dir = os.path.join(
        conf["out_yivo_dir"]
    )  # folder to write yivo enriched files
    if not os.path.exists(path_to_yivo_ttl):
        context.log.info("RDF file doesn't exist, enrichment aborted.")
        yield Output(False, "result")
        return

    if not os.path.exists(out_yivo_dir):
        os.makedirs(out_yivo_dir, exist_ok=True)

    make_yivo_enrichment(in_dir, out_yivo_dir, path_to_yivo_ttl, logger)

    for enriched_fn in sorted(glob.glob(f"{out_yivo_dir}/*xml")):
        out_files.append(LocalFileHandle(enriched_fn))

    yield Output(out_files, "result")


if __name__ == "__main__":
    preset_name = sys.argv[1]
    pipeline: PipelineDefinition = rdf_enrichment_pipeline
    test_preset: PresetDefinition = next(
        filter(
            lambda preset: preset.name == preset_name.split(os.sep)[1],
            pipeline.preset_defs,
        )
    )

    result: PipelineExecutionResult = execute_pipeline(
        pipeline=pipeline, run_config=test_preset.run_config
    )

    assert result.success
