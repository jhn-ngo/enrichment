from dagster import repository

from dagster.core.definitions.decorators import solid, pipeline
from dagster import (
    ExpectationResult,
    List,
    LocalFileHandle,
    Nothing,
)

import configparser
import os, sys
import json
import tempfile
import glob
# Add current directory to the path to load pipelines correctly
sys.path.append(os.path.join(os.path.dirname(__file__)))

p_dirs = {
    "rdf": "conf/pipelines/rdf",
}

from dagster import PresetDefinition
from dagster.utils.yaml_utils import merge_yamls
from moai.tools import update_moai


# Defines list of presets (configs) - each preset contains common.yaml and dataset-specific config
def preset_defs(dirs: list, exclude_common=False) -> list:
    commons = []
    if not exclude_common:
        for p_dir in dirs:
            commons.append(os.path.join(os.getcwd(), f'{p_dir}/_common.yaml'))

    preset_map = {}
    for p_dir in dirs:
        for f in filter(lambda f: '_common' not in f, glob.glob(os.path.join(os.getcwd(), f'{p_dir}/*.yaml'))):
            name = os.path.basename(f).replace(".yaml", "")
            # TODO We need to exclude for example validation presets for dumps from presets for harvest
            if name not in preset_map:
                preset_map[name] = [] + commons
            preset_map[name].append(f)

    presets = []
    for p_name, env_files in preset_map.items():
        presets.append(PresetDefinition(p_name, merge_yamls(env_files)))

    return presets


def update_db(context, edm_files: List[LocalFileHandle]) -> Nothing:
    yield ExpectationResult(label="num_edm_files", success=len(edm_files) > 0)
    config = configparser.ConfigParser()
    config.read(context.solid_config["settings"])
    # Update provider section
    # TODO: update database section
    config["app:jhn"]["provider"] = "edm://{}/*.xml".format(
        os.path.dirname(edm_files[0].path)
    )
    settings_file = os.path.join(tempfile.gettempdir(), "settings.ini")
    with open(settings_file, "w") as cf:
        config.write(cf)

    # Get set name from input files directory
    out_set = os.path.basename(os.path.dirname(os.path.dirname(edm_files[0].path)))
    # update_moai works with command line arguments so keep the original ones before the processing and then
    # restore them at the end
    directus_params = ""
    if "directus" in context.solid_config:
        # dict expected
        directus_config: dict = context.solid_config["directus"]
        directus_config["auth_pwd"] = directus_config.pop("auth_pwds").get(
            directus_config.get("auth_email"), ""
        )
        json.dumps(directus_config)
        directus_params = f'--directus {json.dumps(directus_config).replace(" ", "")}'

    sys_argv = sys.argv
    sys.argv = (
        f"update_moai -v -d --set {out_set} --config "
        f"{settings_file} {directus_params} jhn".split()
    )
    context.log.info("Update command: {}".format(" ".join(sys.argv)))
    update_moai()


@pipeline(preset_defs=preset_defs([p_dirs["rdf"], p_dirs["validation"]]))
def rdf_enrichment_pipeline():
    from enrichment import rdf_enrichment, yivo_enrichment

    return update_db(yivo_enrichment(rdf_enrichment()))

@repository(name="judaica")
def repository():
    return {
        # Note that we can pass a function, rather than pipeline instance.
        # This allows us to construct pipelines on demand.
        "pipelines": {
            "rdf_enrichment_pipeline": lambda: rdf_enrichment_pipeline,
        }
    }
