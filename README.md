# README #

This repository is part of Judaica Europeana 2.0 project. It contains metadata enrichment data integration pipeline (based on Dagster). The pipeline integrates data in TTL format delivered from [Judaica metadata enrichment repo](https://github.com/ubffm/judaica-europeana-2-0)


### How do I get set up? ###

* Set up virtual Pyton 3.6+ environment and install requirements.txt

### How do I run metadata enrichment? ###

* The enrichment is run separately per dataset. The following guidelines refer to JTSA dataset as an example
* Clone [Judaica data repo](https://github.com/ubffm/judaica-europeana-2-0) and copy JTSA*.ttl files from Contextualization_GUF_Persons and Contextualization_GUF_Spatial to out/JTSA/rdf 
* Run pipeline to integrate TTL 
	
dagster judaica execute -c conf/rdf/JTSA.yaml 

* The database will be updated with metadata enrichment extracted from relevant TTL files


